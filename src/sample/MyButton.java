package sample;

import javafx.scene.control.Button;
import javafx.scene.text.Font;

/**
 * Created by osman on 8/31/17.
 */
public class MyButton extends Button{
    public MyButton(){
        setStyle("-fx-background-color: #8b8b8b;" +
                " -fx-border-radius: 20px;");
        setFont(Font.font(40));
        setPrefHeight(100);
        setPrefWidth(100);
    }
}
