package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.Pane;

import java.util.ArrayList;

public class MyBoard extends Pane {
    private ArrayList<MyButton> myButtons = new ArrayList<>();
    private XO currentState = XO.O;
    public MyBoard(){
       restart();
    }
    private void validateFields() {
        equals(myButtons.get(0), myButtons.get(1), myButtons.get(2));
        equals(myButtons.get(3), myButtons.get(4), myButtons.get(5));
        equals(myButtons.get(6), myButtons.get(7), myButtons.get(8));

        equals(myButtons.get(0), myButtons.get(3), myButtons.get(6));
        equals(myButtons.get(1), myButtons.get(4), myButtons.get(7));
        equals(myButtons.get(2), myButtons.get(5), myButtons.get(8));

        equals(myButtons.get(0), myButtons.get(4), myButtons.get(8));
        equals(myButtons.get(6), myButtons.get(4), myButtons.get(2));

    }
    private void equals(MyButton b1, MyButton b2, MyButton b3){
        if (b1.getText().equals("")){
            return;
        }

        if (b1.getText().equals(b2.getText()) && b1.getText().equals(b3.getText())){
            b1. setStyle("-fx-background-color: #00FF00;" +
                    " -fx-border-radius: 20px;");
            b2. setStyle("-fx-background-color: #00FF00;" +
                    " -fx-border-radius: 20px;");
            b3. setStyle("-fx-background-color: #00FF00;" +
                    " -fx-border-radius: 20px;");
            onWin();
        }
    }
    private void onWin(){
        for (MyButton myButton : myButtons){
            myButton.setOnAction(null);
        }
    }
    public void restart(){
        getChildren().removeAll(myButtons);
        myButtons.clear();
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                MyButton myButton = new MyButton();
                myButton.setId(String.valueOf(myButtons.size()));
                myButton.setLayoutX(j * 105);
                myButton.setLayoutY(i * 105);
                myButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if (!validateNextClick(myButton)){
                            return;
                        }
                        if (currentState == XO.X){
                            myButton.setText("O");
                            currentState = XO.O;
                        } else {
                            myButton.setText("X");
                            currentState = XO.X;
                        }
                        validateFields();
                    }
                });
                myButtons.add(myButton);
                getChildren().add(myButton);
            }
        }
    }
    private boolean validateNextClick(MyButton myButton){
        return myButton.getText().equals("");
    }
}
