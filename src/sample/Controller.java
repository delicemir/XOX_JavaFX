package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable{
    public MyBoard myBoard;
    public Button restart;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        restart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                myBoard.restart();
            }
        });
    }
}
